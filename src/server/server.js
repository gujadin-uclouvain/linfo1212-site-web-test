const express = require('express');
const consolidate = require('consolidate');
const MongoClient = require('mongodb').MongoClient;
const session = require('express-session');
const bodyParser = require('body-parser');
const https = require('https');
const fs = require('fs');
const app = express();

app.engine('html', consolidate.hogan);
app.set('view engine', 'html');
app.set('views','../webapp/views');

app.use(express.static('../webapp'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
	secret: "L0uv@!n-LA-n€UvEs",
	resave: false,
	saveUninitialized: true,
	cookie: {
		path: '/',
		httpOnly: true,
	}
}));

const url = 'mongodb://localhost:27017';
const dbName = "lln";
const options = { useNewUrlParser: true, useUnifiedTopology: true };
MongoClient.connect(url, options, (err, client) => {
    	if (err) throw err;
		db = client.db(dbName);
		console.log(`Connected MongoDB: ${url}`);
        console.log(`Database Name: ${dbName}`);
        console.log('-------------------------');
	}
);

// FUNCTIONS SECTION - START
function user_displayed(current_username) {
	if (current_username === undefined) {
		return "Se Connecter";
	}
	return "Bienvenue, " + current_username;
}

function setup_date_main() {
	let d = new Date();
	let day = d.getDate();
	let month = d.toLocaleString('default', { month: 'long' });
	let year = d.getFullYear();
	return day + " " + month + " " + year;
}

function setup_date_incident() {
	let d = new Date();
	let data_date = [d.getDate(), d.getMonth() + 1, d.getHours(), d.getMinutes(), d.getFullYear()];
	for (let i = 0; i < data_date.length-1; i++) {
		if (data_date[i].toString().length === 1) {
			data_date[i] = "0" + data_date[i];
		}
	}
	return data_date[0] + "/" + data_date[1] + "/" + data_date[4] + " | " + data_date[2] + ":" + data_date[3];
}

function id_maker() {
	let d = new Date();
	let list_date = [d.getMilliseconds(), d.getFullYear(), d.getMonth() + 1, d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds() ];
	for (let i = 2; i < list_date.length; i++) {
		if (list_date[i].toString().length === 1) {
			list_date[i] = "0" + list_date[i];
		}
	}
	if (list_date[0].length < 3) {
		if (list_date[0].length === 2) { list_date[0] = "0" + list_date[0]; }
		else { list_date[0] = "00" + list_date[0]; }
	}
	return "" + list_date[1] + list_date[2] + list_date[3] + list_date[4] + list_date[5] + list_date[6] + list_date[0];
}
// FUNCTIONS SECTION - END

app.get('/main',function(req, res) {
	let research = req.query.search;

	db.collection('incidents').findOne( {} , (err, datas) => {
	    if (err) throw err;
	    if (datas === null) {
	    	db.collection('incidents').createIndex( {description: "text", adress: "text"} );
	    	return res.render('main.html', {
				date: setup_date_main(),
				current_username: user_displayed(req.session.current_username),
				is_incidents: false,
                not_found: "Il semble n'y avoir aucun incident à Louvain-La-Neuve en ce moment..."
			});
		}

		let data = [];
	    let cursor;
	    if (research === undefined || research === "") {
	    	cursor = db.collection('incidents').find({}).sort({_id: -1});
		} else {
	    	if (req.query.is_sentence !== undefined) {
	    		research = '\"' + research + '\"'
			}
			cursor = db.collection('incidents').find( {$text: { $search: research, $language: "fr" } } ).sort({_id: -1});
		}

		cursor.forEach(function (doc) {
		    data.push(doc);
		}, function () {
			if (data.length === 0) {
				res.render('main.html', {
					date: setup_date_main(),
					current_username: user_displayed(req.session.current_username),
					is_incidents: false,
					not_found: "Aucun incident ne correspond aux termes de recherche spécifiés<br>(" + research + ")"
				});
			} else {
				res.render('main.html', {
					date: setup_date_main(),
					current_username: user_displayed(req.session.current_username),
					is_incidents: true,
					incidents_list: data,
					description: data.description,
					adress: data.adress,
					writer: data.writer,
					post_date: data.post_date
				});
			}
		});
	});
});

app.get('/subscribe',function(req, res) {
	res.render('subscribe.html', {current_username: user_displayed(req.session.current_username)});
});

app.post('/subscribe/checking', function(req, res) {
	db.collection('users').findOne( {"username" : req.body.user} , (err, data_user) => {
	    if (err) throw err;
	    if (data_user === null) {
	    	console.log(">> '"+ req.body.user + "' doesn't exist in the database");
			res.redirect('/subscribe');
		} else {
	    	if (req.body.pswd === data_user.pswd) {
				req.session.current_username = req.body.user;
				console.log(">> " + req.body.user + " is connected");
				res.redirect('/add_incident');
			} else {
	    		console.log(">> Wrong Password");
				res.redirect('/subscribe');
			}
		}
	});
});

app.post('/subscribe/creating_account', function(req, res) {
	if (req.body.create_username !== "" && req.body.create_pswd !== "" &&
		req.body.create_flname !== "" && req.body.create_email !== "") {

		db.collection('users').findOne( {"username" : req.body.create_username} , (err, data_user) => {
			if (err) throw err;
			if (data_user === null) {
				db.collection('users').insertOne({
					"username": req.body.create_username,
					"pswd": req.body.create_pswd,
					"flname": req.body.create_flname,
					"email": req.body.create_email
				});
				req.session.current_username = req.body.create_username;
				console.log(">> " + req.body.create_username + " has created a new account");
				res.redirect('/add_incident');
			} else {
				console.log(">> '" + req.body.create_username + "' already exist in the database");
				res.redirect('/subscribe');
			}
		});
	} else {
		res.redirect('/subscribe');
	}
});

app.post('/disconnected',function(req, res) {
	req.session.current_username = undefined;
	res.redirect('/main');
});

app.get('/add_incident',function(req, res) {
	if (req.session.current_username) {
		res.render('add_incident.html', {current_username: user_displayed(req.session.current_username)});
	} else {
		res.redirect('/subscribe')
	}
});

app.post('/add_incident/sending',function(req, res) {
	if (req.session.current_username && req.body.description !== "" && req.body.adress !== "") {
		let date = setup_date_incident();
		let to_insert = {
			_id: id_maker(),
			description: req.body.description,
			adress: req.body.adress,
			writer: req.session.current_username,
			post_date: date
		};
		db.collection('incidents').insertOne(to_insert);
		console.log(">> Incident added by " + req.session.current_username);
		res.redirect('/main')
	} else {
		res.redirect('/add_incident');
	}
});

app.use(express.static('src'));
https.createServer({
  key: fs.readFileSync('./key.pem'),
  cert: fs.readFileSync('./cert.pem'),
  passphrase: 'lsinf1212'
}, app).listen(8080);
console.log('HTTPS Express server started on port 8080');
