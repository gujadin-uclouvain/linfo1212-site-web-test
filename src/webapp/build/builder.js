function disable_parts_subscribe() {
    if (document.getElementById("username_connected").innerText === "Se Connecter") {
        document.getElementById("connected_state").style.display = "inline";
    } else {
        document.getElementById("not_connected_state").style.display = "inline";
    }
}

function confirm_subscribe() {
    return window.confirm("Êtes-vous sûr de vouloir vous enregistrer avec ces données ?");
}

function confirm_add_incident() {
    return window.confirm("Êtes-vous sûr de vouloir envoyer cet incident ?\n(Il ne pourra plus être modifié par la suite)");
}